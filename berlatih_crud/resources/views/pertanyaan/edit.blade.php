@extends('template.master')
@section('content')
<div class="ml-3 mr-3">
    <h2>Update Pertanyaan {{$pertanyaan->id}}</h2>
        <form action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="judul">Judul</label>
                <input type="hidden" name="tanggal_dibuat" value="{{$pertanyaan->tanggal_dibuat}}" id="tanggal_dibuat">
                <input type="text" class="form-control" name="judul" value="{{old('judul',$pertanyaan->judul)}}" id="judul" placeholder="Masukkan Judul">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="isi">Isi</label>
                <input type="text" class="form-control" name="isi" value="{{old('isi',$pertanyaan->isi)}}" id="isi" placeholder="Masukkan Isi">
                @error('isi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection
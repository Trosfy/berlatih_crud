@extends('template.master')
@section('content')
<div class="ml-3 mr-3">
    <h2>Pertanyaan {{$pertanyaan->id}}</h2>
    <h4>{{$pertanyaan->judul}}</h4>
    <p>{{$pertanyaan->isi}}</p>
</div>
@endsection
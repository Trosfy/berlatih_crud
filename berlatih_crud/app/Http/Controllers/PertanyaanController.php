<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PertanyaanController extends Controller
{
    //
    public function index()
    {
        $pertanyaans = DB::table('pertanyaan')->get();
        // dd($pertanyaans);
        return view('pertanyaan.index', compact('pertanyaans'));
    }
    public function create()
    {
        return view('pertanyaan.create');
    }
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required',
        ]);
        $query = DB::table('pertanyaan')->insert([
            'judul' => $request['judul'],
            'isi' => $request['isi'],
            'tanggal_dibuat' => now(),
            'tanggal_diperbaharui' => now(),
            'jawaban_tepat_id' => null,
            'profil_id' => null,
        ]);
        return redirect('/pertanyaan');
    }
    public function show($id)
    {
        $pertanyaan = DB::table('pertanyaan')->select('id','judul', 'isi')->where('id',$id)->first();
        return view('pertanyaan.show', compact('pertanyaan'));
    }
    public function edit($id)
    {
        $pertanyaan = DB::table('pertanyaan')->select('id','judul', 'isi', 'tanggal_dibuat')->where('id',$id)->first();
        return view('pertanyaan.edit', compact('pertanyaan'));
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required',
        ]);
        $query = DB::table('pertanyaan')->where('id',$id)->update([
            'judul' => $request['judul'],
            'isi' => $request['isi'],
            'tanggal_dibuat' => $request['tanggal_dibuat'],
            'tanggal_diperbaharui' => now(),
        ]);
        return redirect('/pertanyaan');
    }
    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan');
    }
}
